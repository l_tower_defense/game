package ru.latynin.ltd.screens

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Align
import kotlinx.coroutines.launch
import ktx.actors.onClick
import ktx.async.KtxAsync
import ktx.async.newSingleThreadAsyncContext
import ktx.scene2d.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.latynin.ltd.GameManager
import ru.latynin.ltd.animations.AnimatedObject
import ru.latynin.ltd.animations.AnimatedObject.Companion.waitEndOfAnim
import ru.latynin.ltd.animations.animator
import ru.latynin.ltd.currencies.CurrenciesManager
import ru.latynin.ltd.levels.LevelsManager

class MainMenuScreen(
        val gameManager: GameManager,
        val currenciesManager: CurrenciesManager
): BaseScreen(), KoinComponent, AnimatedObject {

    val logo: Image
    val btnWidth: Float
    val btnHeight: Float
    val leftPanel: Table
    val rightPanel: Table
    var mainPanel: Table
    var lDis = false

    val currenciesPanel = CurrenciesPanel(currenciesManager, this)

    val window: Table

    init {
        rootStage.actors {
            window = table { root ->
//                align(Align.top)
                height = rootStage.height
                btnWidth = rootStage.width*0.4f
                btnHeight = rootStage.height*0.1f
                padTop(rootStage.height*0.15f)
                logo = image("logo") {
                    scaleBy(0.4f)
                    setOrigin(width/2, height/2)
                    centerX(parent)
                    onClick {
                        println("logo")
                        currenciesManager.details.add(10_300)
                    }
                }
                row()
                mainPanel = table {
                    padTop(rootStage.height*0.15f)
                    leftPanel = table {
//                        background = SpriteDrawable(Sprite(Texture("img/b1.png")).apply { setAlpha(0.9f) })
                        padTop(10f)
                        padBottom(10f)
                        padRight(5f)
                        addButton(label = "+50% 30m", image = "cpu", flip = true, width = btnWidth, height = btnHeight) {
                            currenciesManager.memory.add(500f)
                            println("${currenciesManager.memory.value} - ${currenciesManager.memory.currentType}")
                            logo.animator.moveFromActorPosAnim(100F, 0F)
                        }
                        row()
                        addButton(label = "Inventory", image = "box", flip = true, width = btnWidth, height = btnHeight) {
                            gameManager.openInventory(closeWithCallback = this@MainMenuScreen)
                        }
                        row()
                        addButton(label = "Settings", image = "screwdriver", flip = true, width = btnWidth, height = btnHeight) {
                            currenciesManager.memory.add(-0.5f, 1)
                            println("${currenciesManager.memory.value} - ${currenciesManager.memory.currentType}")
                            logo.animator.moveBackAnim()
                        }
                        row()
                        addButton(label = "Wiki", image = "book", flip = true, width = btnWidth, height = btnHeight) {
                            currenciesManager.memory.add(50.5f, 1)
                            gameManager.dispose()
                        }
                    }.cell(padRight = rootStage.width*0.2f)
                    rightPanel = table {
//                        background = SpriteDrawable(Sprite(Texture("img/b1.png")).apply { setAlpha(0.9f) })
                        padTop(10f)
                        padBottom(10f)
                        padLeft(5f)
                        addButton(label = "Play", width = btnWidth, height = btnHeight) {
                            gameManager.openLevelMap(closeWithCallback = this@MainMenuScreen) {
                                currenciesPanel.panel.zIndex = 100
                            }
                        }
                        row()
                        addButton(label = "Research", width = btnWidth, height = btnHeight) {
                            gameManager.clearLevels()
                        }
                    }
                }
            }
        }
        currenciesPanel.panel.apply {
            rootStage.addActor(this)
            zIndex = 0
        }
    }

    override fun updateDefaultSettings(){
        logo.animator.updateDefaultSettings()
        leftPanel.animator.updateDefaultSettings()
        rightPanel.animator.updateDefaultSettings()
    }

    override fun show() {
        super.show()
        gameManager.show(this)
    }

    override fun dispose() {
        gameManager.dispose()
        super.dispose()
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        gameManager.resize(rootStage)
    }

    override fun render(delta: Float) {
        super.render(delta)
        gameManager.act(delta)
    }

    private val executor = newSingleThreadAsyncContext()
    //Menu buttons and logo
    override fun open(endAnimCall: (() -> Unit)) {
        leftPanel.isVisible = true
        rightPanel.isVisible = true
        logo.isVisible = true
        val animations = listOfNotNull(
                leftPanel.animator.moveBackAnim(duration = 0.2f),
                rightPanel.animator.moveBackAnim(duration = 0.2f),
                logo.animator.scaleBackAnim(duration = 0.2f)
        )
        KtxAsync.launch(executor) {
            waitEndOfAnim(animations)
            endAnimCall()
        }
    }
    override fun close(endAnimCall: (() -> Unit)) {
        val animations = listOfNotNull(
                leftPanel.let {
                    it.animator.moveFromDefPosAnim(-it.width, 0f, duration = 0.2f)
                },
                rightPanel.let {
                    it.animator.moveFromDefPosAnim(it.width, 0f, duration = 0.2f)
                },
                logo.animator.reduceAnim(duration = 0.2f)
        )

        KtxAsync.launch(executor) {
            waitEndOfAnim(animations)
            leftPanel.isVisible = false
            rightPanel.isVisible = false
            logo.isVisible = false
            endAnimCall()
        }
    }
}

fun KTableWidget.addButton(
        label: String,
        image: String? = null,
        flip: Boolean = false,
        width: Float = 100f,
        height: Float = 100f,
        click: () -> Unit
) {
    imageButton(if (flip) "flip" else defaultStyle) {
        horizontalGroup {
            image?.let { image(it) }
            center()
            label(label)
        }.cell(width = width, height = height)
        onClick {
            click()
        }
    }
}