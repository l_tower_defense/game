package ru.latynin.ltd.screens.inventory

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Align
import ktx.actors.onClick
import ktx.scene2d.*
import ru.latynin.ltd.items.BoxEvent
import ru.latynin.ltd.items.InventoryManager
import ru.latynin.ltd.items.Box
import ru.latynin.ltd.screens.ItemUtility
import ru.latynin.ltd.screens.getItemView

class BoxTab(
        private val inventoryManager: InventoryManager,
        private val inventoryWindow: InventoryWindow,
        private val backgroundWidth: Float,
        private val backgroundHeight: Float
) {

    val content: KTableWidget
    private val scrollContent: KScrollPane
    lateinit var boxBar: ProgressBar
    val boxMap: MutableMap<Box, KTableWidget>
    lateinit var boxTime: Label
    lateinit var cancelBoxTime: Label
    var needBoxRecreate = true
    private val completedBox: MutableList<Box> = mutableListOf()
    val panel: KTableWidget = scene2d.table {
        height = backgroundHeight * 0.85f
        width = backgroundWidth * 0.85f
        setOrigin(width / 2, height / 2)
        scrollContent = scrollPane {
            fadeScrollBars = true
            setScrollingDisabled(true, false)

            //Box inventory
            content = table {
                pad(0f, backgroundWidth * 0.015f, 0f, 0f)
                align(Align.topLeft)
                boxMap = inventoryManager.getBoxes().map { box ->
                    box to getItemView(
                            skin = inventoryWindow.mainSkin,
                            item = box,
                            width = backgroundWidth * 0.15f,
                            label = getFormattedTime(box.timeToOpenInMin)
                    ) {
                        inventoryManager.removeBox(box)
                        inventoryManager.startOpenBox(box)
                    }
                }.toMap().toMutableMap()
                inventoryManager.addBoxListeners { box, event ->
                    when (event) {
                        BoxEvent.ADD -> {
                            val item = getItemView(
                                    skin = inventoryWindow.mainSkin,
                                    item = box,
                                    width = backgroundWidth * 0.15f,
                                    label = getFormattedTime(box.timeToOpenInMin)
                            ) {
                                inventoryManager.removeBox(box)
                                inventoryManager.startOpenBox(box)
                            }
                            item.findActor<Label>("title").setFontScale(0.8f)
                            boxMap[box] = item
                        }
                        BoxEvent.REMOVE -> boxMap.remove(box)
                    }
                    if (isVisible) inventoryWindow.recreateGrid(InvTab.BOXES)
                    else needBoxRecreate = true
                }
            }
        }.cell(align = Align.top, height = backgroundHeight * 0.65f, width = backgroundWidth * 0.85f)
        row()
        image("separator") {
        }.cell(height = backgroundHeight * 0.04f, width = backgroundWidth * 0.85f,
                padLeft = backgroundWidth * 0.01f, padTop = backgroundHeight * 0.01f)
        row()
        table {
            align(Align.left)
            table {
                background = inventoryWindow.mainSkin.getDrawable("n-block-b")
                val img = image("cpu") {
                    onClick {
                        if (completedBox.isNotEmpty()) {
                            val cachedRewards = completedBox.map { inventoryManager.getReward(it) }.flatten()
                            completedBox.clear()
                            inventoryWindow.window.stage.addActor(scene2d.table { rw ->
                                setFillParent(true)
                                background = SpriteDrawable(Sprite(Texture("img/b2.png")).apply { color = Color(1f, 1f, 1f, 0.5f) })
                                table {
                                    background = SpriteDrawable(Sprite(Texture("img/b1.png")))
                                    pad(backgroundWidth * 0.05f)
                                    cachedRewards.forEach { reward ->
                                        getItemView(
                                                skin = inventoryWindow.mainSkin,
                                                item = reward.item,
                                                width = backgroundWidth * 0.15f,
                                                label = reward.count.toString()
                                        ).cell(padLeft = backgroundWidth * 0.03f)
                                    }
                                    label("X") {
                                        color = Color.BLUE
                                        onClick {
                                            rw.remove()
                                        }
                                    }.cell(align = Align.top, padLeft = backgroundWidth * 0.03f)
                                }
                            })
                            inventoryManager.removeOpenedBox()
                            cachedRewards.forEach {
                                it.item.add(it.count)
                            }
                        }
                    }
                }
                inventoryManager.addOpenBoxListeners { box, type ->
                    when (type) {
                        BoxEvent.ADD -> {
                            background = ItemUtility.getBackground(box.type, inventoryWindow.mainSkin)
                            img.drawable = inventoryWindow.mainSkin.getDrawable(box.id)
                            cancelBoxTime.setText("X")
                        }
                        BoxEvent.REMOVE -> {
                            background = inventoryWindow.mainSkin.getDrawable("n-block-b")
                            img.drawable = inventoryWindow.mainSkin.getDrawable("cpu")
                            boxBar.value = 0f
                            boxTime.setText("")
                            cancelBoxTime.setText("")
                        }
                    }
                }
                inventoryManager.addCompleteOpenBoxListeners { box ->
                    cancelBoxTime.setText("")
                    completedBox.add(box)
                    BoxNotification(
                            inventoryWindow.gameManager.mainMenuScreen.rootStage, box, inventoryWindow.gameManager).run()
                }
                center()
            }.cell(width = backgroundWidth * 0.25f, height = backgroundWidth * 0.25f, padLeft = backgroundWidth * 0.05f)
            table {
                boxBar = progressBar(min = 0f, max = 100f, step = 1f, vertical = false, skin = inventoryWindow.mainSkin) {
                }.cell(padLeft = backgroundWidth * 0.05f, width = backgroundWidth * 0.4f, align = Align.left)
                cancelBoxTime = label("X") {
                    onClick {
                        inventoryManager.cancelOpenBox()
                    }
                }.cell(padLeft = backgroundWidth * 0.02f, align = Align.left)
                row()
                boxTime = label("").cell(padLeft = backgroundWidth * 0.05f, align = Align.left)
            }
        }.cell(height = backgroundHeight * 0.2f, width = backgroundWidth * 0.85f)
    }

    private fun getFormattedTime(minute: Int): String {
        return "${(minute / 60).let {
            if (it > 0) "${it}h " else ""
        }}${(minute % 60).let { if (it > 0) "${it}m" else "" }}"
    }

}