package ru.latynin.ltd.screens.inventory

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.Align
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import ktx.async.KtxAsync
import ktx.async.newSingleThreadAsyncContext
import ktx.scene2d.KTableWidget
import ktx.scene2d.scene2d
import ktx.scene2d.table
import ru.latynin.ltd.GameManager
import ru.latynin.ltd.animations.AnimatedObject
import ru.latynin.ltd.animations.animator
import ru.latynin.ltd.items.Box
import ru.latynin.ltd.screens.getItemView

class BoxNotification(
        val stage: Stage,
        val box: Box,
        val gameManager: GameManager
) : Runnable {
    private val executor = newSingleThreadAsyncContext()
    private val zexecutor = newSingleThreadAsyncContext()
    override fun run() {
        KtxAsync.launch(executor) {
            val item: KTableWidget
            val boxView = scene2d.table {
                isVisible = false
                width = this@BoxNotification.stage.width
                height = this@BoxNotification.stage.height*0.75f
                padBottom(height)
                align(Align.topLeft)
                item = getItemView(
                        skin = skin,
                        item = box,
                        width = width * 0.10f,
                        label = "!"
                ) {
                    gameManager.openInventory(simpleClose = gameManager.allWindow, invTab = InvTab.BOXES)
                }
            }
            stage.addActor(boxView)
            Thread.sleep(1000)
            boxView.pack()
            item.pack()
            boxView.layout()
            item.animator.updateDefaultSettings()
            var isClose = false
            val zSetter = KtxAsync.async(zexecutor) {
                while (!isClose){
                    boxView.zIndex = 100
                    Thread.sleep(100)

                }
            }
            item.setPosition(-item.width,  boxView.height-item.height)
            val anim = item.animator.moveBackAnim()
            zSetter.start()
            boxView.isVisible = true
            AnimatedObject.waitEndOfAnim(listOf(anim))
            Thread.sleep(2000)
            val endAnim = item.animator.moveFromActorPosAnim(-item.width, 0f)
            AnimatedObject.waitEndOfAnim(listOf(endAnim))
            boxView.remove()
            isClose = true
        }
    }

}