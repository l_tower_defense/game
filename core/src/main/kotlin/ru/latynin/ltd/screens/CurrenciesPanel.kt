package ru.latynin.ltd.screens

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Align
import ktx.actors.onClick
import ktx.scene2d.*
import ru.latynin.ltd.currencies.CurrenciesManager

class CurrenciesPanel(
        val currenciesManager: CurrenciesManager,
        val mainMenuScreen: MainMenuScreen
) {
    val memoryLabel: Label
    val cpuPowerLabel: Label
    val detailsLabel: Label

    val name = "currencies"

    val panel = scene2d.table {
        align(Align.top)
        setFillParent(true)
        name = name
        container {
            align(Align.topLeft)
            table {
                background = SpriteDrawable(Sprite(Texture("img/b1.png")).apply { setAlpha(0.9f) })
                padBottom(5f)
                padRight(5f)
                val cpu = skin.getSprite("cpu")
                image("cpu").cell(width = cpu.width*1.5f, height = cpu.height*1.5f, padLeft = 5f, padTop = 5f)
                cpuPowerLabel = label("1.0 Hz") {
                    color = Color.RED
                }.cell(align = Align.left, padLeft = 5f)
                currenciesManager.cpuPower.addListener { value, type, _, _ ->
                    cpuPowerLabel.setText("${String.format("%.2f", value)} ${currenciesManager.cpuPower.types[type]}")
                }
                row()
                val hdd = skin.getSprite("hdd")
                image("hdd").cell(width = hdd.width*1.5f, height = hdd.height*1.5f, padLeft = 5f, padTop = 5f)
                memoryLabel = label("0.0 B") {
                    color = Color.GREEN
                }.cell(align = Align.left, padLeft = 5f)
                currenciesManager.memory.addListener { value, type, _, _ ->
                    memoryLabel.setText("${String.format("%.2f", value)} ${currenciesManager.memory.types[type]}")
                }
            }
        }.cell(width = mainMenuScreen.rootStage.width/2f)
        container {
            align(Align.topRight)
            table {
                background = SpriteDrawable(Sprite(Texture("img/b1.png")).apply { setAlpha(0.9f) })
                padBottom(5f)
                padLeft(5f)
                align(Align.topRight)
                detailsLabel = label("123,123") {
                    color = Color.BLUE
                    onClick {
                        currenciesManager.details.minus(1_500)
                    }
                }.cell(padRight = 5f, padTop = 5f)
                currenciesManager.details.addListener { value, _ ->
                    detailsLabel.setText(String.format("%,3d", value))
                }
                val nut = skin.getSprite("nut")
                image("nut").cell(width = nut.width*1.5f, height = nut.height*1.5f, padRight = 5f, padTop = 5f)
            }
        }.cell(width = mainMenuScreen.rootStage.width/2f, align = Align.topRight)
    }

}