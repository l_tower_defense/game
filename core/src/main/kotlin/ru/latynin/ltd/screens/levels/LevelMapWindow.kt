package ru.latynin.ltd.screens.levels

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Align
import ktx.actors.onClick
import ktx.scene2d.*
import ktx.style.get
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.latynin.ltd.GameManager
import ru.latynin.ltd.animations.AnimatedObject
import ru.latynin.ltd.levels.LevelStatus
import ru.latynin.ltd.levels.LevelsManager
import ru.latynin.ltd.screens.center
import javax.swing.LayoutStyle

class LevelMapWindow(
        val gameManager: GameManager,
        val levelsManager: LevelsManager
): KoinComponent, AnimatedObject {

    val mainSkin: Skin by inject()

    val colors = listOf("r","g","b")
    val window = scene2d.table {
        width = gameManager.mainMenuScreen.rootStage.width
        height = gameManager.mainMenuScreen.rootStage.height
        setOrigin(width / 2, height / 2)
        center(gameManager.mainMenuScreen.rootStage)
        background = SpriteDrawable(Sprite(Texture("img/b2.png")).apply { color = Color(1f, 1f, 1f, 0.5f) })
        scrollPane {
            setFillParent(true)
            table {
                val matrix = levelsManager.loadLevelMatrix()
                matrix.toMutableList().apply {
                    add(0, List(first().size) {null})
                    add(List(first().size) {null})
                }.forEach {
                    it.forEach { level ->
                        table { lTable ->
                            if (level != null){
                                val btn: KImageButton
                                //visual
                                val up = image(getLevelLineByStatus(level.status, true)).apply {
                                    cell(width = 30f, height = 30f, colspan = 3)
                                }
                                row()
                                if(level.upperLevel == null){
                                    up.isVisible = false
                                }
                                val left = image(getLevelLineByStatus(level.status)).apply {
                                    cell(width = 30f, height = 30f)
                                }
                                if(level.leftLevel == null){
                                    left.isVisible = false
                                }
                                table {
                                    btn = addLevelButton(
                                            label = level.number.toString(),
                                            style = getLevelByStatus(level.status),
                                            width = 60f,
                                            height = 60f
                                    ){ btn ->
                                        when(level.status){
                                            LevelStatus.OPEN -> level.setStatus(LevelStatus.COMPLETED_1)
                                            LevelStatus.COMPLETED_1 -> level.setStatus(LevelStatus.COMPLETED_2)
                                            LevelStatus.COMPLETED_2 -> level.setStatus(LevelStatus.COMPLETED_3)
                                            LevelStatus.COMPLETED_3 -> level.setStatus(LevelStatus.COMPLETED_4)
                                        }
                                        println("${level.number} - ${level.status.name}")
                                        levelsManager.updateLevelsStatus()
                                    }
                                }

                                val right = image(getLevelLineByStatus(level.status)).apply {
                                    cell(width = 30f, height = 30f)
                                }
                                if(level.rightLevel == null){
                                    right.isVisible = false
                                }
                                row()
                                val down = image(getLevelLineByStatus(level.status, true)).apply {
                                    cell(width = 30f, height = 30f, colspan = 3)
                                }
                                if(level.lowerLevel == null){
                                    down.isVisible = false
                                }
                                level.addStatusListener { updateLevelStatus(level.status, btn, up, left, right, down) }
                            }
                        }.cell(width = 120f, height = 120f)
                    }
                    row()
                }
            }
        }
    }

    val btnBack = scene2d.table {
        label("<=").apply {
            setFontScale(2f)
            onClick { gameManager.closeLevelMap(openWithCallback = gameManager.mainMenuScreen) }
        }.cell(padLeft = 50f, padBottom = 30f)
    }

    private fun updateLevelStatus(status: LevelStatus,
                                  level: KImageButton,
                                  up: Image, left: Image, right: Image, lower: Image){
        level.style = mainSkin[getLevelByStatus(status)]
        up.drawable = mainSkin.getDrawable(getLevelLineByStatus(status, true))
        left.drawable = mainSkin.getDrawable(getLevelLineByStatus(status))
        right.drawable = mainSkin.getDrawable(getLevelLineByStatus(status))
        lower.drawable = mainSkin.getDrawable(getLevelLineByStatus(status, true))
    }

    private fun getLevelLineByStatus(levelStatus: LevelStatus, rotate: Boolean = false): String =
            when(levelStatus){
                LevelStatus.CLOSE -> "line-n${if (rotate) "-r" else ""}"
                LevelStatus.BLOCKED -> "line-r${if (rotate) "-r" else ""}"
                LevelStatus.OPEN -> "line-b${if (rotate) "-r" else ""}"
                LevelStatus.COMPLETED_1,
                LevelStatus.COMPLETED_2,
                LevelStatus.COMPLETED_3,
                LevelStatus.COMPLETED_4 -> "line-g${if (rotate) "-r" else ""}"
            }

    private fun getLevelByStatus(levelStatus: LevelStatus): String =
            when(levelStatus){
                LevelStatus.CLOSE -> "lvl-close"
                LevelStatus.BLOCKED -> "lvl-blocked"
                LevelStatus.OPEN -> "lvl-open"
                LevelStatus.COMPLETED_1 -> "lvl-complete-1"
                LevelStatus.COMPLETED_2 -> "lvl-complete-2"
                LevelStatus.COMPLETED_3 -> "lvl-complete-3"
                LevelStatus.COMPLETED_4 -> "lvl-complete-4"
            }

    override fun updateDefaultSettings() {
        //TODO
    }

    override fun close(endAnimCall: () -> Unit) {
        btnBack.remove()
        window.remove()
        endAnimCall()
    }

    override fun open(endAnimCall: () -> Unit) {
        gameManager.mainMenuScreen.rootStage.addActor(btnBack)
        endAnimCall()
    }

}

fun KTableWidget.addLevelButton(
        label: String,
        style: String,
        width: Float = 100f,
        height: Float = 100f,
        click: (KImageButton) -> Unit
): KImageButton {
    return imageButton(style) {
        horizontalGroup {
            center()
            label(label)
        }.cell(width = width, height = height)
        onClick {
            click(this)
        }
    }
}