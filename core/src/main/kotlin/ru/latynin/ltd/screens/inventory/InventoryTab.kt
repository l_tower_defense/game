package ru.latynin.ltd.screens.inventory

import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Align
import ktx.scene2d.*
import ru.latynin.ltd.items.InventoryManager
import ru.latynin.ltd.items.Item
import ru.latynin.ltd.screens.getItemView

class InventoryTab(
        private val inventoryManager: InventoryManager,
        private val inventoryWindow: InventoryWindow,
        private val backgroundWidth: Float,
        private val backgroundHeight: Float
) {

    val content: KTableWidget
    val itemMap: Map<Item, KTableWidget>
    var needRecreate = true
    val panel: KScrollPane = scene2d.scrollPane {
        fadeScrollBars = true
        setScrollingDisabled(true, false)
        height = backgroundHeight*0.9f
        width = backgroundWidth * 0.85f
        //Item inventory
        content = table {
            pad(0f, backgroundWidth * 0.015f, 0f, 0f)
            align(Align.topLeft)
            itemMap = inventoryManager.items.map { item ->
                val element = getItemView(
                        skin = inventoryWindow.mainSkin,
                        item = item,
                        width = backgroundWidth * 0.15f,
                        label = item.id
                ){
                    item.minus(11)
                }
                item.addListener { newValue, oldValue ->
                    element.findActor<Label>("title").setText(newValue.toString())
                    if (newValue == 0 || oldValue == 0) {
                        if (isVisible) inventoryWindow.recreateGrid(InvTab.INVENTORY)
                        else needRecreate = true
                    }
                }
                item to element
            }.toMap()
        }
    }

}