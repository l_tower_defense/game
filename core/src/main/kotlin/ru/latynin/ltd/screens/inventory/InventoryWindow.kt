package ru.latynin.ltd.screens.inventory

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align
import kotlinx.coroutines.launch
import ktx.actors.isShown
import ktx.actors.onClick
import ktx.async.KtxAsync
import ktx.async.newSingleThreadAsyncContext
import ktx.scene2d.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.latynin.ltd.GameManager
import ru.latynin.ltd.animations.AnimatedObject
import ru.latynin.ltd.animations.AnimatedObject.Companion.waitEndOfAnim
import ru.latynin.ltd.animations.animator
import ru.latynin.ltd.items.InventoryManager
import ru.latynin.ltd.items.Box
import ru.latynin.ltd.items.ColorType
import ru.latynin.ltd.screens.center
import java.time.Instant

enum class InvTab {
    INVENTORY,
    BOXES,
    CRAFT
}

class InventoryWindow(
        val gameManager: GameManager,
        val inventoryManager: InventoryManager
) : KoinComponent, AnimatedObject {

    val mainSkin: Skin by inject()

    private val uLine: Image
    private val hLine: Image
    private val dLine: Image

    private val tabR: Image
    private val tabG: Image
    private val tabB: Image

    private val title: Label

    var currentTab = InvTab.INVENTORY
        private set

    private val backgroundWidth = gameManager.mainMenuScreen.rootStage.width * 0.70f
    private val backgroundHeight = backgroundWidth * 1.89f

    val content: KTableWidget

    //Inv Tab
    private val inventoryTab: InventoryTab = InventoryTab(inventoryManager, this, backgroundWidth, backgroundHeight)
    //Box Tab
    private val boxTab: BoxTab = BoxTab(inventoryManager, this, backgroundWidth, backgroundHeight)
    //Craft Tab
    // TODO

    val window = scene2d.table { inv ->
        width = gameManager.mainMenuScreen.rootStage.width
        height = gameManager.mainMenuScreen.rootStage.height
        setOrigin(width / 2, height / 2)
        center(gameManager.mainMenuScreen.rootStage)
        val lineWidth = backgroundWidth * 0.1f
        val lineHeight = backgroundWidth * 0.85f

        //lines
        table {
            uLine = image("u-line-g") {
            }.cell(width = lineHeight, height = lineWidth,
                    padLeft = lineWidth,
                    align = Align.left)
        }.cell(colspan = 2, align = Align.left, padTop = height * 0.10f)
        row()
        table {
            hLine = image("h-line-g") {
            }.cell(width = lineWidth, height = backgroundHeight, align = Align.top)
        }

        //inventory
        table {
            zIndex = 10
            setBackground("inventory-background")

            align(Align.topLeft)
            title = label(InvTab.INVENTORY.name) {
                onClick {
                    inventoryManager.items.random().add(1)
                    inventoryManager.addBox(
                            listOf(
                                    Box(id = "s-box", type = ColorType.BLUE, timeToOpenInMin = 1),
                                    Box(id = "s-box", type = ColorType.GREEN, timeToOpenInMin = 20),
                                    Box(id = "s-box", type = ColorType.RED, timeToOpenInMin = 45),
                                    Box(id = "m-box", type = ColorType.BLUE, timeToOpenInMin = 45),
                                    Box(id = "m-box", type = ColorType.GREEN, timeToOpenInMin = 60),
                                    Box(id = "m-box", type = ColorType.RED, timeToOpenInMin = 85),
                                    Box(id = "b-box", type = ColorType.BLUE, timeToOpenInMin = 105),
                                    Box(id = "b-box", type = ColorType.GREEN, timeToOpenInMin = 120),
                                    Box(id = "b-box", type = ColorType.RED, timeToOpenInMin = 145)
                            ).random()
                    )
                }
            }.cell(
                    padLeft = backgroundWidth * 0.08f,
                    padTop = backgroundHeight * 0.018f,
                    padBottom = backgroundHeight * 0.022f,
                    align = Align.left)
            row()

            //content
            content = table {
                //Items
                //Boxes
                //Craft //TODO
            }.cell(width = backgroundWidth * 0.85f, height = backgroundHeight*0.9f)

            table {
                align(Align.top)
                image("open-box") {
                    onClick {
                        if (currentTab != InvTab.INVENTORY) {
                            openStorage(InvTab.INVENTORY)
                        }
                    }
                }.cell(row = true)
                image("close-box") {
                    onClick {
                        if (currentTab != InvTab.BOXES) {
                            openStorage(InvTab.BOXES)
                        }
                    }
                }.cell(padTop = backgroundHeight * 0.019f, row = true)
                image("craft") {
                    onClick {
                        if (currentTab != InvTab.CRAFT) {
                            openStorage(InvTab.CRAFT)
                        }
                    }
                }.cell(padTop = backgroundHeight * 0.019f)
            }.cell(width = backgroundWidth * 0.11f, height = backgroundHeight * 0.91f, padLeft = backgroundWidth*0.03f, padTop = backgroundHeight * 0.01f)

        }.cell(width = backgroundWidth, height = backgroundHeight)

        //lines
        table {
            zIndex = 1
            align(Align.topLeft)
            tabG = image("tab-g").cell(row = true)
            tabB = image("tab-b").cell(padTop = backgroundHeight * 0.019f, row = true)
            tabR = image("tab-r").cell(padTop = backgroundHeight * 0.019f)
        }.cell(width = backgroundWidth * 0.07f, height = backgroundHeight * 0.91f, padTop = backgroundHeight * 0.09f, align = Align.topLeft)
        row()
        table {
            zIndex = 1
            dLine = image("d-line-g") {
            }.cell(width = lineHeight * 0.85f, height = lineWidth,
                    padBottom = height * 0.10f, padLeft = lineWidth,
                    align = Align.topLeft)
            imageButton("close") {
                onClick {
                    gameManager.closeInventory(openWithCallback = gameManager.mainMenuScreen)
                }
            }.cell(height = backgroundHeight * 0.07f, width = backgroundHeight * 0.07f, padRight = backgroundWidth * 0.15f)
        }.cell(colspan = 2)

        recreateGrid(InvTab.INVENTORY)
        recreateGrid(InvTab.BOXES)
    }

    var timeon: Float = 0f
    fun act(delta: Float){
        timeon += delta
        if(inventoryManager.currentOpenBox != null && timeon > 1 && boxTab.panel.isShown()){
            val n1 = inventoryManager.currentOpenBox!!.endOpenTime!!.minusSeconds(Instant.now().epochSecond).epochSecond
            val n3 = ((inventoryManager.currentOpenBox!!.timeToOpenInMin*60)/100f)
            boxTab.boxBar.value = 100-n1/n3
            if(boxTab.boxBar.value < 100){
                boxTab.boxTime.setText("${n1/60/60}:${(n1/60)%60}:${n1%60}")
            } else {
                boxTab.boxTime.setText("Complete")
            }
            println(boxTab.boxBar.value)
            timeon = 0f
        }
    }

    override fun updateDefaultSettings() {
        listOf(tabG, tabR, tabB).forEach {
            it.x -= it.width
            it.animator.defPosition.set(it.x, it.y)
        }
        uLine.apply {
            y -= height
            animator.defPosition.set(x, y)
        }
        hLine.apply {
            x += width
            animator.defPosition.set(x, y)
        }
        dLine.apply {
            y += height
            animator.defPosition.set(x, y)
        }
    }

    private fun<T> KTableWidget.grid(map: Map<T, KTableWidget>, col: Int = 3, filter: ((T) -> Boolean) = {true}) {
        map.entries
                .filter { filter(it.key) }
                .forEachIndexed { index, entry ->
                    add(entry.value).apply {
                        pad(backgroundWidth * 0.015f)
                    }
                    if ((index + 1) % col == 0) {
                        row()
                    }
                }
    }

    fun recreateGrid(tab: InvTab) {
        when (tab) {
            InvTab.INVENTORY -> {
                inventoryTab.needRecreate = false
                inventoryTab.content.clearChildren()
                inventoryTab.content.grid(inventoryTab.itemMap, 3){ it.value > 0 }
                inventoryTab.content.layout()
            }
            InvTab.BOXES -> {
                boxTab.needBoxRecreate = false
                boxTab.content.clearChildren()
                boxTab.content.grid(boxTab.boxMap, 3)
                boxTab.content.layout()
            }
            InvTab.CRAFT -> inventoryTab //TODO
        }
    }

    override fun open(endAnimCall: (() -> Unit)) {
        openStorage(currentTab, endAnimCall)
    }

    fun openStorage(tab: InvTab, endCloseFun: (() -> Unit) = {}) {
        closeInvLine {
            content.clearChildren()
            val color = when (tab) {
                InvTab.INVENTORY -> {
                    inventoryTab.panel.center(content)
                    content.addActor(inventoryTab.panel)
                    if(inventoryTab.needRecreate) {
                        recreateGrid(tab)
                    }
                    Pair(tabG, "g")
                }
                InvTab.BOXES -> {
                    boxTab.panel.center(content)
                    content.addActor(boxTab.panel)
                    if(boxTab.needBoxRecreate) {
                        recreateGrid(tab)
                    }
                    Pair(tabB, "b")
                }
                InvTab.CRAFT -> Pair(tabR, "r")
            }
            uLine.drawable = mainSkin.getDrawable("u-line-${color.second}")
            hLine.drawable = mainSkin.getDrawable("h-line-${color.second}")
            dLine.drawable = mainSkin.getDrawable("d-line-${color.second}")
            title.setText(tab.name)
            currentTab = tab
            endCloseFun()
            openInvLine()
        }
        closeInvTab {
            openInvTab(when (tab) {
                InvTab.INVENTORY -> tabG
                InvTab.BOXES -> tabB
                InvTab.CRAFT -> tabR
            })
        }
    }

    override fun close(endAnimCall: (() -> Unit)) {
        closeInvTab()
        closeInvLine {
            endAnimCall()
        }
    }


    //Animations
    private val executor = newSingleThreadAsyncContext()

    //Inventory Lines
    private fun openInvLine(endAnimCall: (() -> Unit) = {}) {
        val animations = listOfNotNull(
                uLine.let {
                    it.animator.moveFromDefPosAnim(0f, it.height, duration = 0.2f, clearAction = false)
                },
                hLine.let {
                    it.animator.moveFromDefPosAnim(-it.width, 0f, duration = 0.2f, clearAction = false)
                },
                dLine.let {
                    it.animator.moveFromDefPosAnim(0f, -it.height, duration = 0.2f, clearAction = false)
                }
        )
        KtxAsync.launch(executor) {
            waitEndOfAnim(animations)
            endAnimCall()
        }
    }

    private fun closeInvLine(endAnimCall: (() -> Unit) = {}) {
        val animations = listOfNotNull(
                uLine.animator.moveBackAnim(duration = 0.2f, clearAction = false),
                dLine.animator.moveBackAnim(duration = 0.2f, clearAction = false),
                hLine.animator.moveBackAnim(duration = 0.2f, clearAction = false)
        )
        KtxAsync.launch(executor) {
            waitEndOfAnim(animations)
            endAnimCall()
        }
    }

    private fun closeInvTab(endAnimCall: (() -> Unit) = {}) {
        val animations = listOf(
                tabG.animator.moveBackAnim(duration = 0.2f, clearAction = false),
                tabB.animator.moveBackAnim(duration = 0.2f, clearAction = false),
                tabR.animator.moveBackAnim(duration = 0.2f, clearAction = false)
        )
        KtxAsync.launch(executor) {
            waitEndOfAnim(animations)
            endAnimCall()
        }
    }

    private fun openInvTab(tab: Actor, endAnimCall: (() -> Unit) = {}) {
        val animation = tab.animator.moveFromDefPosAnim(tab.width, 0f, duration = 0.2f, clearAction = false)
        KtxAsync.launch(executor) {
            waitEndOfAnim(listOf(animation))
            endAnimCall()
        }
    }


}