package ru.latynin.ltd.screens

import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.Drawable
import com.badlogic.gdx.utils.Align
import ktx.actors.onClick
import ktx.scene2d.KTableWidget
import ktx.scene2d.image
import ktx.scene2d.label
import ktx.scene2d.table
import ru.latynin.ltd.common.TypedPersistEntity
import ru.latynin.ltd.items.ColorType

class ItemUtility {
    companion object {
        fun getBackground(type: ColorType, skin: Skin): Drawable = when (type) {
            ColorType.RED -> skin.getDrawable("item-bg-r")
            ColorType.GREEN -> skin.getDrawable("item-bg-g")
            ColorType.BLUE -> skin.getDrawable("item-bg-b")
        }
    }
}



fun KTableWidget.getItemView(skin: Skin, item: TypedPersistEntity, width: Float, label: String, call: (() -> Unit) = {}): KTableWidget {
    return table {
        background = ItemUtility.getBackground(item.type, skin)
        image(item.id).cell(
                width = width,
                height = width,
                pad = width * 0.33f,
                padBottom = 0f)
        onClick {
            call()
        }
        row()
        label(label).cell(padBottom = width * 0.05f, align = Align.bottom).name = "title"
    }
}


