package ru.latynin.ltd

import com.badlogic.gdx.scenes.scene2d.Stage
import ru.latynin.ltd.animations.AnimatedObject
import ru.latynin.ltd.common.GameSaver
import ru.latynin.ltd.items.InventoryManager
import ru.latynin.ltd.currencies.CurrenciesManager
import ru.latynin.ltd.levels.LevelStatus
import ru.latynin.ltd.levels.LevelsManager
import ru.latynin.ltd.screens.*
import ru.latynin.ltd.screens.inventory.InvTab
import ru.latynin.ltd.screens.inventory.InventoryWindow
import ru.latynin.ltd.screens.levels.LevelMapWindow

class GameManager(
        private val gameSaver: GameSaver,
        private val currenciesManager: CurrenciesManager,
        private val inventoryManager: InventoryManager,
        private val levelsManager: LevelsManager
) {

    val mainMenuScreen = MainMenuScreen(this, currenciesManager)
    val inventoryWindow = InventoryWindow(this, inventoryManager)
    val levelsWindow = LevelMapWindow(this, levelsManager)

    val allWindow = listOf<AnimatedObject>(mainMenuScreen, inventoryWindow, levelsWindow)

    fun clearLevels(){
        levelsManager.levels.forEach {
            it.setStatus(LevelStatus.CLOSE)
        }
        levelsManager.updateLevelsStatus()
    }

    fun show(screen: BaseScreen){
        gameSaver.loadAll()
        levelsManager.updateLevelsStatus()

        screen.rootStage.addActor(inventoryWindow.window)
        screen.rootStage.addActor(mainMenuScreen.window)

        screen.rootStage.draw()
        mainMenuScreen.updateDefaultSettings()
        inventoryWindow.updateDefaultSettings()

        inventoryWindow.window.remove()
    }

    fun act(delta: Float){
        inventoryWindow.act(delta)
    }

    fun dispose() {
        gameSaver.saveAll()
        inventoryManager.dispose()
    }

    fun resize(stage: Stage) {
        mainMenuScreen.window.center(stage)
        inventoryWindow.window.center(stage)
    }

    fun openLevelMap(simpleClose: List<AnimatedObject> = emptyList(),
                     closeWithCallback: AnimatedObject? = null,
                     endAnimCall: (() -> Unit) = {}
    ){
        simpleClose.forEach { it.close() }
        if(closeWithCallback != null){
            closeWithCallback.close {
                mainMenuScreen.rootStage.addActor(levelsWindow.window)
                levelsWindow.open {
                    endAnimCall()
                }
            }
        }else {
            mainMenuScreen.rootStage.addActor(levelsWindow.window)
            levelsWindow.open {
                endAnimCall()
            }
        }
    }

    fun closeLevelMap(
            simpleOpen: List<AnimatedObject> = emptyList(),
            openWithCallback: AnimatedObject? = null,
            endAnimCall: (() -> Unit) = {}
    ) {
        if (mainMenuScreen.rootStage.actors.indexOf(levelsWindow.window) < 0) {
            return
        }
        simpleOpen.forEach { it.open() }
        if(openWithCallback != null){
            levelsWindow.close {
                levelsWindow.window.remove()
                openWithCallback.open(endAnimCall)
            }
        } else {
            levelsWindow.close {
                levelsWindow.window.remove()
            }
        }
    }

    fun openInventory(
            simpleClose: List<AnimatedObject> = emptyList(),
            closeWithCallback: AnimatedObject? = null,
            invTab: InvTab? = null,
            endAnimCall: (() -> Unit) = {}
    ){
        simpleClose.forEach { it.close() }
        if(closeWithCallback != null){
            closeWithCallback.close {
                mainMenuScreen.rootStage.addActor(inventoryWindow.window)
                if(invTab != null) inventoryWindow.openStorage(invTab) {
                    endAnimCall()
                } else {
                    inventoryWindow.open {
                        endAnimCall()
                    }
                }
            }
        }else {
            mainMenuScreen.rootStage.addActor(inventoryWindow.window)
            if(invTab != null) inventoryWindow.openStorage(invTab) {
                endAnimCall()
            } else {
                inventoryWindow.open {
                    endAnimCall()
                }
            }
        }
    }

    fun closeInventory(
            simpleOpen: List<AnimatedObject> = emptyList(),
            openWithCallback: AnimatedObject? = null,
            endAnimCall: (() -> Unit) = {}
    ) {
        if (mainMenuScreen.rootStage.actors.indexOf(inventoryWindow.window) < 0) {
            return
        }
        simpleOpen.forEach { it.open() }
        if(openWithCallback != null){
            inventoryWindow.close {
                inventoryWindow.window.remove()
                openWithCallback.open(endAnimCall)
            }
        } else {
            inventoryWindow.close {
                inventoryWindow.window.remove()
            }
        }
    }
}