package ru.latynin.ltd.common

import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.badlogic.gdx.utils.JsonWriter
import ktx.json.fromJson

inline fun<reified T> Json.fromListJson(data: String): List<T> =
    fromJson<List<JsonValue>>(data).map { fromJson<T>(it.toJson(JsonWriter.OutputType.json)) }
