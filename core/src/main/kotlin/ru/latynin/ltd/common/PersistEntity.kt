package ru.latynin.ltd.common

import ru.latynin.ltd.items.ColorType

interface PersistEntity {

    val id: String

}

interface TypedPersistEntity: PersistEntity {

    val type: ColorType

}