package ru.latynin.ltd.items

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.badlogic.gdx.utils.JsonWriter
import ktx.json.JsonSerializer
import ktx.json.fromJson
import ru.latynin.ltd.common.GameSaver
import java.lang.RuntimeException
import java.time.Instant
import java.util.*
import kotlin.concurrent.schedule
import kotlin.math.ceil

enum class BoxEvent {
    ADD,
    REMOVE
}
class BoxReward(
        val item: Item,
        val count: Int
)
class InventoryManager(
        private val gameSaver: GameSaver,
        private val skin: Skin
) {

    val items: List<Item> = listOf(
            Item("resistor", ColorType.BLUE),
            Item("diode", ColorType.BLUE),
            Item("capacitor", ColorType.GREEN),
            Item("led", ColorType.GREEN),
            Item("photodiode", ColorType.GREEN),
            Item("transistor", ColorType.RED),
            Item("phototransistor", ColorType.RED)
    )
    private val boxes: MutableList<Box> = mutableListOf()
    fun getBoxes(): List<Box> = boxes
    private val boxListeners: MutableList<(Box, BoxEvent)->Unit> = mutableListOf()
    var currentOpenBox: Box? = null
        private set
    private val openBoxListeners: MutableList<(Box, BoxEvent)->Unit> = mutableListOf()
    private val completeOpenBoxListeners: MutableList<(Box)->Unit> = mutableListOf()
    private var boxTimer: TimerTask? = null
    private val timer: Timer = Timer()

    val savedKey = "inventory"
    init {
        gameSaver.addClassTab<InventoryManager>("inventory")
        gameSaver.addClassTab<Item>("item")
        gameSaver.addClassTab<Box>("box")
        gameSaver.addClassTab<List<Item>>("items")
        gameSaver.addClassTab<List<Box>>("boxes")

        gameSaver.addSerializer(object : JsonSerializer<InventoryManager> {
            override fun read(json: Json, jsonValue: JsonValue, type: Class<*>?): InventoryManager {
                (json.fromJson(jsonValue.get("items").toJson(JsonWriter.OutputType.json)) as List<JsonValue>).map {
                    json.fromJson<Item>(it.toJson(JsonWriter.OutputType.json))
                }
                this@InventoryManager.boxes.clear()
                (json.fromJson(jsonValue.get("boxes").toJson(JsonWriter.OutputType.json)) as List<JsonValue>).forEach {
                    addBox(json.fromJson(it.toJson(JsonWriter.OutputType.json)))
                }
                jsonValue.get("currentBox")?.let {
                    if(!it.isNull){
                        startOpenBox(json.fromJson(it.toJson(JsonWriter.OutputType.json)))
                    }
                }
                return this@InventoryManager
            }

            override fun write(json: Json, value: InventoryManager, type: Class<*>?) {
                json.writeObjectStart()
                json.writeValue("items", items)
                json.writeValue("boxes", boxes)
                json.writeValue("currentBox", currentOpenBox)
                json.writeObjectEnd()
            }
        })
        gameSaver.addSerializer(object : JsonSerializer<Item> {
            override fun read(json: Json, jsonValue: JsonValue, type: Class<*>?): Item {
                return items.find { jsonValue.getString("id") == it.id }?.apply {
                    set(jsonValue.getInt("count"))
                } ?: throw RuntimeException()
            }

            override fun write(json: Json, value: Item, type: Class<*>?) {
                json.writeObjectStart()
                json.writeValue("id", value.id)
                json.writeValue("count", value.value)
                json.writeObjectEnd()
            }

        })
        gameSaver.addSerializer(object : JsonSerializer<Box> {
            override fun read(json: Json, jsonValue: JsonValue, type: Class<*>?): Box {
                return Box(
                        id = jsonValue.getString("id"),
                        type = ColorType.valueOf(jsonValue.getString("type")),
                        timeToOpenInMin = jsonValue.getInt("timeToOpen"),
                        endOpenTime = jsonValue.get("endTimeToOpen").let {
                            if (it.isNull) null
                            else Instant.ofEpochSecond(it.asLong())
                        }
                )
            }

            override fun write(json: Json, value: Box, type: Class<*>?) {
                json.writeObjectStart()
                json.writeValue("id", value.id)
                json.writeValue("type", value.type.toString())
                json.writeValue("timeToOpen", value.timeToOpenInMin)
                json.writeValue("endTimeToOpen", value.endOpenTime?.epochSecond)
                json.writeObjectEnd()
            }

        })
        gameSaver.addObjectToSave(savedKey, this)
        gameSaver.addLoadAllFunction {
            it.loadObject<InventoryManager>(savedKey)
        }

        items.forEach {
            skin.add(it.id, Sprite(Texture("img/items/${it.id}.png")))
            skin.add(it.id + "-s", Sprite(Texture("img/items/${it.id}-s.png")))
        }
    }

    fun dispose(){
        timer.cancel()
    }

    fun addBox(box: Box){
        boxes.add(box)
        boxListeners.forEach { it(box, BoxEvent.ADD) }
    }
    fun removeBox(box: Box){
        boxes.remove(box)
        boxListeners.forEach { it(box, BoxEvent.REMOVE) }
    }
    fun addBoxListeners(call: (Box, BoxEvent) -> Unit){
        boxListeners.add(call)
    }
    fun addOpenBoxListeners(call: (Box, BoxEvent) -> Unit){
        openBoxListeners.add(call)
    }
    fun addCompleteOpenBoxListeners(call: (Box) -> Unit){
        completeOpenBoxListeners.add(call)
    }

    fun getReward(box: Box): List<BoxReward> = listOf(BoxReward(items.random(), 10), BoxReward(items.random(), 20), BoxReward(items.random(), 15))

    fun startOpenBox(box: Box): Instant{
        if (currentOpenBox != null) {
            return currentOpenBox!!.endOpenTime!!
        }
        box.endOpenTime = box.endOpenTime ?: Date().toInstant().plusSeconds(box.timeToOpenInMin*60L)
        currentOpenBox = box
        boxTimer = timer.schedule(Date.from(currentOpenBox!!.endOpenTime!!)){
            completeOpenBoxListeners.forEach { it(currentOpenBox!!) }
        }
        openBoxListeners.forEach { it(box, BoxEvent.ADD) }
        return box.endOpenTime!!
    }

    fun boxIsOpen(): Boolean{
        if(currentOpenBox == null) return false
        return Instant.now().epochSecond > currentOpenBox!!.endOpenTime?.epochSecond ?: 0
    }

    fun removeOpenedBox(): Boolean{
        if(!boxIsOpen()) return false
        openBoxListeners.forEach { it(currentOpenBox!!, BoxEvent.REMOVE) }
        currentOpenBox = null
        boxTimer?.cancel()
        return true
    }

    fun cancelOpenBox(): Boolean{
        if(currentOpenBox == null || boxIsOpen()) return false
        currentOpenBox!!.timeToOpenInMin = ceil((currentOpenBox!!.endOpenTime!!.epochSecond - Instant.now().epochSecond)/60f).toInt()
        currentOpenBox!!.endOpenTime = null
        addBox(currentOpenBox!!)
        boxTimer?.cancel()
        openBoxListeners.forEach { it(currentOpenBox!!, BoxEvent.REMOVE) }
        currentOpenBox = null
        return true
    }

}