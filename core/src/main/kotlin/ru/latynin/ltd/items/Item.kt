package ru.latynin.ltd.items

import ru.latynin.ltd.common.PersistEntity
import ru.latynin.ltd.common.TypedPersistEntity
import ru.latynin.ltd.currencies.QuantitativeValue
import java.time.Instant

enum class ColorType{
    RED,
    GREEN,
    BLUE
}

class Item(
        override val id: String,
        override val type: ColorType
): QuantitativeValue(), TypedPersistEntity {

    constructor(id: String, type: ColorType, value: Int) : this(id, type) {
        this.value = value
    }

}

class Box(
        override val id: String = "s-box",
        override val type: ColorType = ColorType.BLUE,
        var timeToOpenInMin: Int = 60,
        var endOpenTime: Instant? = null
): TypedPersistEntity