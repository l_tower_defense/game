package ru.latynin.ltd.levels

import org.koin.core.KoinComponent
import org.koin.core.inject

enum class LevelStatus{
    CLOSE,
    BLOCKED,
    OPEN,
    COMPLETED_1,
    COMPLETED_2,
    COMPLETED_3,
    COMPLETED_4
}

class Level(
        val number: Int = 0,
        val upperLevel: Int? = null,
        val rightLevel: Int? = null,
        val lowerLevel: Int? = null,
        val leftLevel: Int? = null,
        val requiredLevels: List<Int> = emptyList(),
        status: LevelStatus = LevelStatus.CLOSE
): KoinComponent {

    val userLevelsInfo by inject<UserLevelsInfo>()

    var status: LevelStatus = status
        private set

    private val statusListeners: MutableList<(LevelStatus)->Unit> = mutableListOf()

    fun addStatusListener(call: (LevelStatus)->Unit){
        statusListeners.add(call)
    }

    fun setStatus(status: LevelStatus){
        this.status = status
        when(status){
            LevelStatus.COMPLETED_1 -> userLevelsInfo.saveLevelStatus(number, 1)
            LevelStatus.COMPLETED_2 -> userLevelsInfo.saveLevelStatus(number, 2)
            LevelStatus.COMPLETED_3 -> userLevelsInfo.saveLevelStatus(number, 3)
            LevelStatus.COMPLETED_4 -> userLevelsInfo.saveLevelStatus(number, 4)
            else -> userLevelsInfo.removeLevel(number)
        }
        statusListeners.forEach { it(status) }
    }
}