package ru.latynin.ltd.levels

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json
import ru.latynin.ltd.common.fromListJson

class LevelsManager(
        val usersLevelsInfo: UserLevelsInfo
){

    private val json = Json()

    lateinit var levels: List<Level>

    fun loadLevelMatrix(): List<List<Level?>> {
        val data = if(!::levels.isInitialized) {
            levels = json.fromListJson(Gdx.files.internal("levels.json").readString())
            levels
        } else levels

        val matrix = buildMatrix(data.toMutableList())
        matrix.reverse()
        println(matrix.joinToString("\n") { row -> row.map { it?.number ?: 0 }.joinToString(" ") })
        return matrix
    }

    private fun buildMatrix(data: MutableList<Level>): MutableList<MutableList<Level?>> {
        class Point(val x: Int, val y: Int)
        val remaining = data.toMutableList()
        val points = mutableMapOf(data.first().number to Point(0, 0))
        val maxSize = remaining.size
        var counter = 0
        while (remaining.isNotEmpty() && counter < maxSize) {
            counter++
            val level = remaining.firstOrNull { it.number in points } ?: continue
            remaining.remove(level)
            val point = points[level.number]!!
            level.upperLevel?.let { points[it] = Point(point.x, point.y + 1) }
            level.lowerLevel?.let { points[it] = Point(point.x, point.y - 1) }
            level.leftLevel?.let { points[it] = Point(point.x - 1, point.y) }
            level.rightLevel?.let { points[it] = Point(point.x + 1, point.y) }
        }
        val otherMatrix = if (remaining.size > 0) buildMatrix(remaining) else emptyList<MutableList<Level?>>()
        val xOffset = -points.values.minBy(Point::x)!!.x
        val yOffset = -points.values.minBy(Point::y)!!.y
        val xMax = points.values.maxBy(Point::x)!!.x + xOffset
        val yMax = points.values.maxBy(Point::y)!!.y + yOffset
        val matrix = Array(yMax + 1) { arrayOfNulls<Level>(xMax + 1) }
        for ((number, point) in points) {
            matrix[point.y + yOffset][point.x + xOffset] = data.find { it.number == number }
        }
        return matrix.map(Array<Level?>::toMutableList).toMutableList().apply {
            otherMatrix.forEach {
                add(it)
            }
        }
    }

    fun updateLevelsStatus(){
        for (levelStatus in usersLevelsInfo.getLevelStatus()) {
            val level = levels.find { it.number == levelStatus.number } ?: continue
            level.setStatus(when(levelStatus.step) {
                1 -> LevelStatus.COMPLETED_1
                2 -> LevelStatus.COMPLETED_2
                3 -> LevelStatus.COMPLETED_3
                4 -> LevelStatus.COMPLETED_4
                else -> LevelStatus.CLOSE
            })
        }
        levels.filter { it.status == LevelStatus.CLOSE }.forEach { closeLevel ->
            if(closeLevel.requiredLevels.all { reqLevel -> levelIsCompleted(reqLevel) }){
                closeLevel.setStatus(LevelStatus.OPEN)
            }
        }
    }

    private fun levelIsCompleted(number: Int): Boolean {
        val level = levels.find { it.number == number } ?: return false

        return level.status == LevelStatus.COMPLETED_1 ||
                level.status == LevelStatus.COMPLETED_2 ||
                level.status == LevelStatus.COMPLETED_3 ||
                level.status == LevelStatus.COMPLETED_4
    }

}