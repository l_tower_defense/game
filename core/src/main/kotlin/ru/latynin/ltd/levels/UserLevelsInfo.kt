package ru.latynin.ltd.levels

import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.badlogic.gdx.utils.JsonWriter
import ktx.json.JsonSerializer
import ktx.json.fromJson
import ru.latynin.ltd.common.GameSaver


class LevelStatusInfo(
        val number: Int = 0,
        var step: Int = 0
)

class UserLevelsInfo(
        private val gameSaver: GameSaver
) {

    private val levelStatus: MutableList<LevelStatusInfo> = mutableListOf()

    val savedKey = "UserLevelsInfo"
    init {
        gameSaver.addClassTab<UserLevelsInfo>("UserLevelsInfo")
        gameSaver.addClassTab<LevelStatusInfo>("levelStatus")
        gameSaver.addSerializer(object : JsonSerializer<UserLevelsInfo> {
            override fun read(json: Json, jsonValue: JsonValue, type: Class<*>?): UserLevelsInfo {
                levelStatus.clear()
                (json.fromJson(jsonValue.get("levels").toJson(JsonWriter.OutputType.json)) as List<LevelStatusInfo>).forEach {
                    levelStatus.add(it)
                }
                return this@UserLevelsInfo
            }

            override fun write(json: Json, value: UserLevelsInfo, type: Class<*>?) {
                json.writeObjectStart()
                json.writeValue("levels", levelStatus)
                json.writeObjectEnd()
            }
        })
        gameSaver.addObjectToSave(savedKey, this)
        gameSaver.addLoadAllFunction {
            it.loadObject<UserLevelsInfo>(savedKey)
        }
    }

    fun getLevelStatus(): List<LevelStatusInfo> = levelStatus
    fun saveLevelStatus(number: Int, step: Int){
        val oldLevel = levelStatus.find { it.number == number }
        if(oldLevel == null) {
            levelStatus.add(LevelStatusInfo(number, step))
            return
        }
        oldLevel.step = step
    }
    fun removeLevel(number: Int): Boolean{
        return levelStatus.removeIf { it.number == number }
    }

}