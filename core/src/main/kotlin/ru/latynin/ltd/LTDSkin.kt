package ru.latynin.ltd

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import ktx.scene2d.defaultStyle
import ktx.style.*

object LTDSkin {

    fun createSkin(): Skin = skin { skin ->
        add(defaultStyle, BitmapFont())
        label {
            font = skin[defaultStyle]
        }
        imageButton {
            up = Image(Texture("img/button.png")).drawable
        }
        imageButton("flip") {
            val sprite = Sprite(Texture("img/button.png"))
            sprite.flip(true, false)
            up = SpriteDrawable(sprite)
        }
        imageButton("close") {
            val sprite = Sprite(Texture("img/inventory/close-button.png"))
            up = SpriteDrawable(sprite)
        }
        textButton {
            font = skin[defaultStyle]
        }
        window {
            titleFont = skin[defaultStyle]
        }
        scrollPane {}
        progressBar(name = "default-horizontal") {
            background = SpriteDrawable(Sprite(Texture("img/inventory/progress-bar-bg.png")))
            knob = SpriteDrawable(Sprite(Texture("img/inventory/progress-bar-knob-l.png")))
            knobBefore = SpriteDrawable(Sprite(Texture("img/inventory/progress-bar-knob.png")))
        }
        add("box", Sprite(Texture("img/box.png")))
        add("book", Sprite(Texture("img/book.png")))
        add("cpu", Sprite(Texture("img/cpu.png")))
        add("hdd", Sprite(Texture("img/hdd.png")))
        add("nut", Sprite(Texture("img/nut.png")))
        add("screwdriver", Sprite(Texture("img/screwdriver.png")))
        add("logo", Sprite(Texture("img/logo.png")))
        add("inventory-background", Sprite(Texture("img/inventory/background.png")))
        add("open-box", Sprite(Texture("img/inventory/open-box.png")))
        add("close-box", Sprite(Texture("img/inventory/close-box.png")))
        add("craft", Sprite(Texture("img/inventory/craft.png")))
        add("separator", Sprite(Texture("img/inventory/separator.png")))



        listOf("r", "g", "b").forEach { color ->
            listOf("h", "u", "d").forEach { dir ->
                add("$dir-line-$color", Sprite(Texture("img/inventory/$dir-line-$color.png")))
            }
            add("tab-$color", Sprite(Texture("img/inventory/tab-$color.png")))
            add("item-bg-$color", Sprite(Texture("img/items/item-bg-$color.png")))
            add("item-bg-$color-s", Sprite(Texture("img/items/item-bg-$color-s.png")))
            add("line-$color", Sprite(Texture("img/levels/line-$color.png")))
            add("line-$color-r", Sprite(Texture("img/levels/line-$color.png")).apply { rotate(90f) })

            listOf("b").forEach {
                add("$color-block-$it", Sprite(Texture("img/blocks/$color-block-$it.png")))
            }
        }
        add("n-block-b", Sprite(Texture("img/blocks/n-block-b.png")))
        add("line-n", Sprite(Texture("img/levels/line-n.png")))
        add("line-n-r", Sprite(Texture("img/levels/line-n.png")).apply { rotate(90f) })
        listOf("s","m","b").forEach {
            add("$it-box", Sprite(Texture("img/box/$it-box.png")))
            add("$it-box-s", Sprite(Texture("img/box/$it-box-s.png")))
        }

        imageButton("lvl-open") {
            up = SpriteDrawable(Sprite(Texture("img/levels/open.png")))
        }
        imageButton("lvl-close") {
            up = SpriteDrawable(Sprite(Texture("img/levels/close.png")))
        }
        imageButton("lvl-blocked") {
            up = SpriteDrawable(Sprite(Texture("img/levels/blocked.png")))
        }
        for(i in 1..4) {
            imageButton("lvl-complete-$i") {
                up = SpriteDrawable(Sprite(Texture("img/levels/complete-$i.png")))
            }
        }
    }

}