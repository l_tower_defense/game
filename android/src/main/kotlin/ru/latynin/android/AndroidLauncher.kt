package ru.latynin.android

import android.os.Build
import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.latynin.ltd.Koin
import ru.latynin.ltd.LTDGame
import ru.latynin.ltd.common.GameSaver

class AndroidLauncher : AndroidApplication(), KoinComponent {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val game = LTDGame()
        Koin.start(game)
        val gameSaver by inject<GameSaver>()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val externalfilesDir = getExternalFilesDir(null)
            if(externalfilesDir!=null) gameSaver.externalFilesDirForAndroid = externalfilesDir.path
        }

        val configuration = AndroidApplicationConfiguration()
        initialize(game, configuration)
    }

    override fun onPause() {
        super.onPause()
        val gameSaver by inject<GameSaver>()
        gameSaver.saveAll()
    }
}